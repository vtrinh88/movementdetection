# Movement Detection #

This project employs [Leap Motion SDK](https://www.leapmotion.com/) for hand tracking, and displays fingers movement. [Video link](https://www.youtube.com/watch?v=_FgbFIrAzIc)

### System Requirements ###

* [Leap sensor](https://www.leapmotion.com/)
* OpenGL

### How do I get set up? ###

Please contact me

### Copyright ###

Viet Trinh © 2015  
vqtrinh@ucsc.edu