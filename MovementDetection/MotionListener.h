//
//  MotionListener.h
//  MovementDetection
//
//  Created by Viet Trinh on 4/10/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifndef __MovementDetection__MotionListener__
#define __MovementDetection__MotionListener__

#include <stdio.h>
#include "Leap.h"
using namespace Leap;

class MotionListener : public Listener{
 public:
    virtual void onInit(const Controller& controller);
    virtual void onConnect(const Controller& controller);
    virtual void onFrame(const Controller& controller);
    virtual void onDisconnect(const Controller& controller);
    virtual void onExit(const Controller& controller);
};

#endif /* defined(__MovementDetection__MotionListener__) */
