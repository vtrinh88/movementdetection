//
//  VTSphere.h
//  MovementDetection
//
//  Created by Viet Trinh on 4/11/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifndef __MovementDetection__VTSphere__
#define __MovementDetection__VTSphere__

#include <stdio.h>
#include "VTVector.h"

class VTSphere{
    float radius;
    int slices;
    int stacks;
    VTVector center;
    VTVector ambient;
  public:
    VTSphere();
    VTSphere(VTVector center, float radius, int slices, int stacks);
    VTSphere(VTVector center, float radius, int slices, int stacks, VTVector ambient);
    void setCenterCoordinates(VTVector center);
    void setRadius(float radius);
    void setSlices(int slices);
    void setStacks(int stacks);
    void setAmbientValues(VTVector ambient);
    void setSlicesAndStacks(int slices, int stacks);
    int getSlices();
    int getStacks();
    float getRadius();
    VTVector getCenter();
    VTVector getAmbientValues();

};

#endif /* defined(__MovementDetection__VTSphere__) */
