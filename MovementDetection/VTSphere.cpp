//
//  VTSphere.cpp
//  MovementDetection
//
//  Created by Viet Trinh on 4/11/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#include "VTSphere.h"

VTSphere::VTSphere(){
    setCenterCoordinates(VTVector(0,0,0));
    setRadius(0);
    setSlicesAndStacks(0, 0);
    setAmbientValues(VTVector(0,0,0));
}

VTSphere::VTSphere(VTVector center, float radius, int slices, int stacks){
    setCenterCoordinates(center);
    setRadius(radius);
    setSlicesAndStacks(slices, stacks);
}

VTSphere::VTSphere(VTVector center, float radius, int slices, int stacks, VTVector ambient){
    setCenterCoordinates(center);
    setRadius(radius);
    setSlicesAndStacks(slices, stacks);
    setAmbientValues(ambient);
}

void VTSphere::setCenterCoordinates(VTVector center){
    this->center.setComponents(center.XComponent(), center.YComponent(), center.ZComponent());
}

void VTSphere::setRadius(float radius){
    this->radius = radius;
}

void VTSphere::setAmbientValues(VTVector ambient){
    this->ambient.setComponents(ambient.XComponent(), ambient.YComponent(), ambient.ZComponent());
}

void VTSphere::setSlices(int slices){
    this->slices = slices;
}

void VTSphere::setStacks(int stacks){
    this->stacks = stacks;
}

void VTSphere::setSlicesAndStacks(int slices, int stacks){
    setSlices(slices);
    setStacks(stacks);
}

VTVector VTSphere::getCenter(){
    return this->center;
}

float VTSphere::getRadius(){
    return this->radius;
}

VTVector VTSphere::getAmbientValues(){
    return this->ambient;
}

int VTSphere::getSlices(){
    return this->slices;
}

int VTSphere::getStacks(){
    return this->stacks;
}
