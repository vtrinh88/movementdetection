//
//  main.cpp
//  MovementDetection
//
//  Created by Viet Trinh on 4/9/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "VTPoint.h"
#include "VTVector.h"
#include "VTSphere.h"
#include "Constants.h"
#include "Leap.h"
#include "MotionListener.h"

using namespace std;
using namespace Leap;

/*----- GLOBAL VARIABLES ----------------------------*/
int    button, state;
float  gx, gy, XWIN, ZWIN, XFLOOR, YFLOOR, ZFLOOR;
float  MATERIAL_AMBIENT[4], MATERIAL_DIFFUSE[4], MATERIAL_SPECULAR[4];
VTPoint  VIEWER, LOOKAT;
VTVector DIRECTION_VIEWER_TO_LOOKAT;
Controller controller;
MotionListener listener;

/*----- FUNCTIONS DECLARATION -----------------------*/
void init();
void setupScreen();
void displayScreen();
void mouseClicks(int but,int sta,int x,int y);
void mouseMoves(int x, int y);
void keyPresses(unsigned char c, int x, int y);
void cleanUp();

void rotateLeftRight(float angle);
void moveForwardBackward(float step);
void stepLeftRight(float step);

void loadMaterialAmbientValues(float red, float green, float blue, float alpha);
void loadMaterialDiffuseValues(float red, float green, float blue, float alpha);
void loadMaterialSpecularValues(float red, float green, float blue, float alpha);
void setMaterialValues();

void drawDisplayFrame();
void drawScene(Frame & frame);
void drawFingerTip(VTVector location);
VTVector convertFromLeapCoordinatesToDisplayCoordinates(Vector vector);

/*----- FUNCTIONS DEFINITION -----------------------*/
int main(int argc, char * argv[]){
    init();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    
    // Create a window
    glutCreateWindow("Motion Tracking");
    glutPositionWindow(0, 0);
    glutReshapeWindow(XWIN, ZWIN);
    
    // Program start here...
    glutDisplayFunc(displayScreen);
    glutMouseFunc(mouseClicks);
    glutMotionFunc(mouseMoves);
    glutKeyboardFunc(keyPresses);
    glutMainLoop();
    cleanUp();
    
    return 0;
}


void init(){
    button = state = 1;
    XWIN   = 1000.0;
    ZWIN   = 600.0;
    XFLOOR = 100.0;
    YFLOOR = 50.0;
    ZFLOOR = 50.0;
    VIEWER.setCoordinates(50.0,40.0,80.0);
    LOOKAT.setCoordinates(static_cast<float>(XFLOOR/2.0),static_cast<float>(YFLOOR/3.0),0.0);
    controller.addListener(listener);
}


void cleanUp(){
    controller.removeListener(listener);
}


void setupScreen(){
    
    /*------ SET UP 3D SCREEN -------*/
    float lightPosition[4] = { 50.0, 100.0, 100.0 , 0.0 };   // x, y, z, w
    float lightAmbient[4]  = { 0.6, 0.6, 0.6, 0.5 };     // r, g, b, a
    float lightDiffuse[4]  = { 0.8, 0.3, 0.3, 0.5 };     // r, g, b, a
    float lightSpecular[4] = { 0.8, 0.3, 0.1, 0.5 };    // r, g, b, a
    
    glClearColor(0.0,0.0,0.0,1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,XWIN/ZWIN,5.0,5000.0);
    glViewport(0,0,XWIN,ZWIN);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    //glEnable(GL_POINT_SMOOTH);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glEnable(GL_LIGHT0);
    
    // viewer is looking towards the center of the terrain
    gluLookAt(VIEWER.XCoord(),VIEWER.YCoord(),VIEWER.ZCoord(),LOOKAT.XCoord(),LOOKAT.YCoord(),LOOKAT.ZCoord(),0,1,0);
    
    /*
    // set up background/terrain color
    float floorTerrainMaterialAmbientDiffuse[4] = { 0.8, 0.8, 0.8, 1.0 }; // r, g, b, a
    float floorTerrainMaterialSpecular[4]       = { 0.8, 0.8, 0.8, 1.0 }; // r, g, b, a
    
    //--- draw the floor terrain ---//
    // xz plane with lower left corner (0,0,0)
    // up direction is y
    //
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, floorTerrainMaterialAmbientDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, floorTerrainMaterialSpecular);
    glBegin(GL_QUADS);
        glNormal3f(0.0,1.0,0.0);
        glVertex3f(0.0,0.0,0.0);
        glVertex3f(XFLOOR,0.0,0.0);
        glVertex3f(XFLOOR,0.0,ZFLOOR);
        glVertex3f(0.0,0.0,ZFLOOR);
    glEnd();*/
}


void displayScreen(){
    
    setupScreen();
    loadMaterialAmbientValues(1.0, 1.0, 1.0, 1.0);
    loadMaterialDiffuseValues(0.9, 0.9, 0.9, 1.0);
    loadMaterialSpecularValues(0.1, 0.1, 0.1, 1.0);
    setMaterialValues();
    
    drawDisplayFrame();
    if (controller.isConnected()) {
        Frame frame = controller.frame();
         if (frame.hands().count() > 0) {
            drawScene(frame);
        }
    }
    
    glutSwapBuffers();
    glutPostRedisplay();
}


void mouseClicks(int but,int sta,int x,int y){
    button = but;
    state = sta;
    
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        
        gx = float(x)/XWIN;
        gy = float(ZWIN-y)/ZWIN;
        
        // code here...
    }
    
    glutPostRedisplay();
}


void mouseMoves(int x, int y){
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        gx = float(x)/XWIN;
        gy = float(ZWIN-y)/ZWIN;
        
        // code here..
        
    }
    
    glutPostRedisplay();
}


void keyPresses(unsigned char c, int x, int y){
    
    if (c =='w')     { moveForwardBackward(10.0);}
    else if (c =='x'){ moveForwardBackward(-10.0);}
    else if (c =='q'){ stepLeftRight(10.0);}
    else if (c =='e'){ stepLeftRight(-10.0);}
    else if (c =='a'){ rotateLeftRight(10*PI/180.0);}
    else if (c =='d'){ rotateLeftRight(-10*PI/180.0);}
    
    //more code here...
    glutPostRedisplay();
}



void moveForwardBackward(float step){
    
    // normalize direction vector
    DIRECTION_VIEWER_TO_LOOKAT.setXComponent(LOOKAT.XCoord() - VIEWER.XCoord());
    DIRECTION_VIEWER_TO_LOOKAT.setYComponent(LOOKAT.YCoord() - VIEWER.YCoord());
    DIRECTION_VIEWER_TO_LOOKAT.setZComponent(LOOKAT.ZCoord() - VIEWER.ZCoord());
    DIRECTION_VIEWER_TO_LOOKAT.normalize();
    
    // translate viewer and look-at-point positions
    VIEWER.setXCoord(VIEWER.XCoord() + step*DIRECTION_VIEWER_TO_LOOKAT.XComponent());
    VIEWER.setZCoord(VIEWER.ZCoord() + step*DIRECTION_VIEWER_TO_LOOKAT.ZComponent());
    LOOKAT.setXCoord(LOOKAT.XCoord() + step*DIRECTION_VIEWER_TO_LOOKAT.XComponent());
    LOOKAT.setZCoord(LOOKAT.ZCoord() + step*DIRECTION_VIEWER_TO_LOOKAT.ZComponent());
}


void stepLeftRight(float step){
    
    // code here to step left right ...
}


void rotateLeftRight(float angle){
    VTVector translateToOrigin, rotate;
    
    // translate to origin
    translateToOrigin.setXComponent(LOOKAT.XCoord() - VIEWER.XCoord());
    translateToOrigin.setYComponent(LOOKAT.YCoord() - VIEWER.YCoord());
    translateToOrigin.setZComponent(LOOKAT.ZCoord() - VIEWER.ZCoord());
    
    // rotate around a pivot
    rotate.setXComponent(translateToOrigin.XComponent()*cos(angle) + translateToOrigin.ZComponent()*sin(angle));
    rotate.setYComponent(translateToOrigin.YComponent());
    rotate.setZComponent(-1*translateToOrigin.XComponent()*sin(angle) + translateToOrigin.ZComponent()*cos(angle));
    
    // translate back
    LOOKAT.setXCoord(rotate.XComponent() + VIEWER.XCoord());
    LOOKAT.setYCoord(rotate.YComponent() + VIEWER.YCoord());
    LOOKAT.setZCoord(rotate.ZComponent() + VIEWER.ZCoord());
}


void setMaterialValues(){
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, MATERIAL_AMBIENT);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, MATERIAL_DIFFUSE);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, MATERIAL_SPECULAR);
}


void loadMaterialAmbientValues(float red, float green, float blue, float alpha){
    MATERIAL_AMBIENT[0]  = red;
    MATERIAL_AMBIENT[1]  = green;
    MATERIAL_AMBIENT[2]  = blue;
    MATERIAL_AMBIENT[3]  = alpha;
}


void loadMaterialDiffuseValues(float red, float green, float blue, float alpha){
    MATERIAL_DIFFUSE[0]  = red;
    MATERIAL_DIFFUSE[1]  = green;
    MATERIAL_DIFFUSE[2]  = blue;
    MATERIAL_DIFFUSE[3]  = alpha;
}


void loadMaterialSpecularValues(float red, float green, float blue, float alpha){
    MATERIAL_SPECULAR[0] = red;
    MATERIAL_SPECULAR[1] = green;
    MATERIAL_SPECULAR[2] = blue;
    MATERIAL_SPECULAR[3] = alpha;
}


void drawDisplayFrame(){
    int intervalCount = 25;
    float XIntervalWidth = XFLOOR/intervalCount;
    float YIntervalWidth = YFLOOR/intervalCount;
    float ZIntervalWidth = ZFLOOR/intervalCount;
    
    /* draw floor frame : XZ plane */
    for (int i=0; i<=intervalCount; i++) {
        glBegin(GL_LINE_STRIP);
            glVertex3f(0.0+i*XIntervalWidth,0.0,0.0);
            glVertex3f(0.0+i*XIntervalWidth,0.0,ZFLOOR);
        glEnd();
        glBegin(GL_LINE_STRIP);
            glVertex3f(0.0,0.0,0.0+i*ZIntervalWidth);
            glVertex3f(XFLOOR,0.0,0.0+i*ZIntervalWidth);
        glEnd();
    }
    
    /* draw wall frame : XY plane */
    for (int i=0; i<=intervalCount; i++) {
        glBegin(GL_LINE_STRIP);
            glVertex3f(0.0+i*XIntervalWidth,0.0,0.0);
            glVertex3f(0.0+i*XIntervalWidth,YFLOOR,0.0);
        glEnd();
        glBegin(GL_LINE_STRIP);
            glVertex3f(0.0,0.0+i*YIntervalWidth,0.0);
            glVertex3f(XFLOOR,0.0+i*YIntervalWidth,0.0);
        glEnd();
    }
}


void drawScene(Frame & frame){
    
    /* Iterate through list of hands */
    HandList hands = frame.hands();
    for (HandList::const_iterator handIterator = hands.begin(); handIterator != hands.end(); handIterator++) {
        const Hand hand = *handIterator;
        //hand.isLeft() ? cout << "Left hand is detected\n" : cout << "Right hand is detected\n";
        
        /* Iterate through list of fingers */
        FingerList fingers= hand.fingers();
        for (FingerList::const_iterator fingerIterator = fingers.begin(); fingerIterator != fingers.end(); fingerIterator++) {
            const Finger finger = *fingerIterator;
            Vector tipPosition = finger.tipPosition();
            //cout << "tip_x=" << tipPosition.x << "; tip_y=" << tipPosition.y << "; tip_z=" << tipPosition.z << endl;
            drawFingerTip(convertFromLeapCoordinatesToDisplayCoordinates(tipPosition));
        }
    }
}


void drawFingerTip(VTVector location){
    VTVector center(location.XComponent(), location.YComponent(), location.ZComponent());
    VTVector ambient(0.54, 0.17, 0.89);
    VTSphere sphere(center, 3, 25, 25, ambient);
    
    loadMaterialAmbientValues(sphere.getAmbientValues().XComponent(),
                              sphere.getAmbientValues().YComponent(),
                              sphere.getAmbientValues().ZComponent(),
                              1.0);
    setMaterialValues();
    
    glPushMatrix();
    glTranslated(sphere.getCenter().XComponent(), sphere.getCenter().YComponent(), sphere.getCenter().ZComponent());
    glutSolidSphere(sphere.getRadius(), sphere.getSlices(), sphere.getStacks());
    glPopMatrix();
}


VTVector convertFromLeapCoordinatesToDisplayCoordinates(Vector leapCoordinates){
    VTVector displayCoordinates, leapCoordRange, displayCoordRange;
    
    leapCoordRange.setComponents(LEAP_COORD_MAX_X - LEAP_COORD_MIN_X,
                                 LEAP_COORD_MAX_Y - LEAP_COORD_MIN_Y,
                                 LEAP_COORD_MAX_Z - LEAP_COORD_MIN_Z);
    
    displayCoordRange.setComponents(DISPLAY_COORD_MAX_X - DISPLAY_COORD_MIN_X,
                                    DISPLAY_COORD_MAX_Y - DISPLAY_COORD_MIN_Y,
                                    DISPLAY_COORD_MAX_Z - DISPLAY_COORD_MIN_Z);
    
    displayCoordinates.setXComponent(DISPLAY_COORD_MIN_X + (leapCoordinates.x - LEAP_COORD_MIN_X)*(displayCoordRange.XComponent()/leapCoordRange.XComponent()));
    displayCoordinates.setYComponent(DISPLAY_COORD_MIN_Y + (leapCoordinates.y - LEAP_COORD_MIN_Y)*(displayCoordRange.YComponent()/leapCoordRange.YComponent()));
    displayCoordinates.setZComponent(DISPLAY_COORD_MIN_Z + (leapCoordinates.z - LEAP_COORD_MIN_Z)*(displayCoordRange.ZComponent()/leapCoordRange.ZComponent()));
    
    return displayCoordinates;
}





