//
//  MotionListener.cpp
//  MovementDetection
//
//  Created by Viet Trinh on 4/10/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#include "MotionListener.h"
#include <iostream>
using namespace std;

void MotionListener::onInit(const Leap::Controller &controller){
    cout << "Initialized Leap" << endl;
}

void MotionListener::onConnect(const Leap::Controller &controller){
    cout << "...Device is connected" << endl;
    controller.enableGesture(Gesture::TYPE_CIRCLE);
    controller.enableGesture(Gesture::TYPE_KEY_TAP);
    controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
    controller.enableGesture(Gesture::TYPE_SWIPE);
}

void MotionListener::onFrame(const Leap::Controller &controller){
    /* Empty body */
}

void MotionListener::onDisconnect(const Leap::Controller &controller){
    cout << "...Device is disconnected" << endl;
}

void MotionListener::onExit(const Leap::Controller &controller){
    cout << "Exit Leap" << endl;
}